package com.example.quiz2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.activity_users.view.*
import kotlinx.android.synthetic.main.user_recycler_layout.view.*

class RecyclerViewAdapter(
    private val users: MutableList<UserModel>,
    private val itemOnClick: ItemOnClick
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        fun onBind() {
            val model = users[adapterPosition]
            itemView.imageView.setImageResource(model.image)
            itemView.usernameTextView.text = model.username
            itemView.emailTextView.text = model.email

            itemView.deleteButton.setOnClickListener(this)
            itemView.editButton.setOnClickListener(this)
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.deleteButton -> {
                    itemOnClick.onClickDelete(adapterPosition)
                }
                R.id.editButton -> {
                    itemOnClick.onClickEdit(adapterPosition)
                }
//                else -> {
//                    itemOnClick.onClick3(adapterPosition)
//                }
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.user_recycler_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


}