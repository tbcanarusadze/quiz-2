package com.example.quiz2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.user_recycler_layout.*

class UserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        init()
    }

    private fun init() {
        saveButton.setOnClickListener {
            if (usernameEditText.text.isEmpty() || emailEditText.text.isEmpty()) {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                saveInfo()
            }
        }
    }

    private fun saveInfo() {
        val userModel = UserModel(
            usernameEditText.text.toString(),
            emailEditText.text.toString()
        )
        val position = intent.extras?.get("position") as Int
        intent.putExtra("position", position)
        intent.putExtra("userModelOutPut", userModel)

        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}

