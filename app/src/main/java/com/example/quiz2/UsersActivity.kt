package com.example.quiz2

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.user_recycler_layout.*

class UsersActivity : AppCompatActivity(){

    private val userList = mutableListOf<UserModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private val position = 0
    private val requestCode1 = 10
    private val requestCode2 = 11

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        setData()
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(userList, object : ItemOnClick {
            override fun onClickDelete(position: Int) {
                removeUser(position)
            }

            override fun onClickEdit(position: Int) {
                editUser(position)
            }

        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        addButton.setOnClickListener {
            val intent = Intent(this, UserActivity::class.java)
            intent.putExtra("position", position)
            startActivityForResult(intent, requestCode1)
        }

    }

    private fun setData() {
        userList.add(
            0,
            UserModel("Ana Rusadze", "ana.rusadze.1@btu.edu.ge")
        )
        userList.add(
            0,
            UserModel("Nika Tabatadze", "nikatabatadze9@gmail.com")
        )

    }

    private fun removeUser(position: Int) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Remove User")
        alert.setMessage("Are you sure you want to remove user?")
        alert.setPositiveButton(
            "Yes"
        ) { _: DialogInterface, _: Int ->
            userList.removeAt(position)
            adapter.notifyItemRemoved(position)
        }
        alert.setNegativeButton("No") { _: DialogInterface, _: Int ->
        }
        alert.show()
    }


    private fun editUser(position: Int) {
        val intent = Intent(this, UserActivity::class.java)
        intent.putExtra("position", position)
        startActivityForResult(intent, requestCode2)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //when we want to add an item
        if (resultCode == Activity.RESULT_OK && requestCode == requestCode1) {
            val userModel = data?.extras?.getParcelable<UserModel>("userModelOutPut")
            val position  = data?.extras?.get("position") as Int
            userList.add(0, UserModel(userModel!!.username, userModel.email))
            adapter.notifyItemInserted(position)
            recyclerView.scrollToPosition(position)

//       when we want to edit an item
        } else if(resultCode == Activity.RESULT_OK && requestCode == requestCode2){
            val userModel = data?.extras?.getParcelable<UserModel>("userModelOutPut")
            val position  = data?.extras?.get("position") as Int
            usernameTextView.text = userModel!!.username
            emailTextView.text = userModel.email
            userList[position] = userModel
            adapter.notifyItemChanged(position)

        }

    }


}
